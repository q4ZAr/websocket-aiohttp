"""
Code taken from: https://github.com/aio-libs/aiohttp/blob/master/examples/server_simple.py

Websocket server can be tested by terminal command:
wscat -c ws://0.0.0.0:8080/ws
"""
from aiohttp import web
from random import randint

shared_with_all_ws_clients = randint(0, 1000)

async def wshandle(request):
    ws = web.WebSocketResponse()
    await ws.prepare(request)

    variable_per_ws_client = randint(0, 1000)

    async for msg in ws:
        variable_per_message = randint(0, 1000)
        if msg.type == web.WSMsgType.text:
            print("variable_shared_with_all_clients ", shared_with_all_ws_clients)
            print("variable_per_ws_client ", variable_per_ws_client)
            print("variable_per_message ", variable_per_message)

            await ws.send_str("Hello, {}".format(msg.data))
        elif msg.type == web.WSMsgType.close:
            break

    return ws


app = web.Application()
app.add_routes([web.get("/ws", wshandle)])
web.run_app(app)
