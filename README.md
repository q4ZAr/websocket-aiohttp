# Hacking/testing idea of using websockets with ML models

Inspiration from video: https://www.youtube.com/watch?v=ZGU_fIOMpgk

## Why websockets?

Maybe websockets could be used to run ML models in production.

Here are some, but not all pros and cons:

Pros:
* With websockets it is possible to create stateful connections. So no need to load ML model weights from file to memory before every client message.
* Maybe `namespace` parameter can be used to divide states. This might be useful on scaling.
* Faster connection. No need to establish TCP connection for every message. Might be handy for example when streaming sound signal to server and getting actions as response.

Cons:
* proxys, virus protection, load balancer, etc can break websocket pipe.

## Demo solution

This is skeleton solution to show how to make websocket pipeline work. Also tests how state is constructed.

m_server.pt has three variables to test how state works.

`shared_with_all_ws_clients`: All established websocket clients print the same number. So this scope should be shared between all the calls. Apparently any websocked cannot edit this variable.

`variable_per_ws_client`: Every websocket clients print different number, but the same client prints the same number every time. This state should be tied to client.

`variable_per_message`: This variable prints new value on every call, so this value is stateless.


### Install & run
* Open three terminals in a location to install solution
* On terminal 1, run:

```
git clone https://gitlab.com/tech-testing/websocket-aiohttp.git
cd websocket-aiohttp
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
python m_server.py
```

* On terminal 2 and 3, run:

```
cd websocket-aiohttp
source venv/bin/activate
python m_client.py
```

* On terminal 2 and 3, write some text and press enter
